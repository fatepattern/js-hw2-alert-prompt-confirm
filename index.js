let firstName;
let age;

while(firstName === null || typeof firstName === 'undefined' || !isNaN(parseInt(firstName)) || firstName === ''){
    firstName = prompt("What is your name", firstName);
}

while(age === null || isNaN(age) || age === ''){
    age = prompt("What is your age", age);
}

if(age < 18) {
    alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22){
    let ageConfirmation = confirm("Are you sure you want to continue?");
    if(ageConfirmation){
        alert("Welcome " + firstName);
    } else{
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome " + firstName);
}